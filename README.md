# It's a me.

This is the fresh code for my own personal/professional website.

From my page you can view a variety of different aspects about me,  
including but not limited to:  
	
	- Resume
	- Online Presences
	- Activities and Organizations

As always, you can view the current state of the [project](http://piperchester.com). 
	
	
